from tkinter import * 
from tkinter.messagebox import *
from tkinter.filedialog import *
from lecture_json import Lecture_json
from requete_api import Requete_api
import tkinter as tk

from pandas import NA
import json

class Interface :
    def __init__(self):
        self.fenetre = Tk()
        self.play = Label(self.fenetre, text= "Choisissez d'abord une liste d'artistes", padx=50, pady=50)
        self.value = Spinbox(self.fenetre, from_=1, to=20)
        self.b = Button(self.fenetre, text = "Choisir la liste des artistes", bg="#BB8FCE")
    
    def afficher_chansons(self, playlist):
        self.play.pack_forget()
        canvas = tk.Canvas(self.fenetre)
        scroll_y = tk.Scrollbar(self.fenetre, orient="vertical", command=canvas.yview)
        frame = tk.Frame(canvas)
        # group of widgets
        for i in range(0, len(playlist)):
            tk.Label(frame, text=str(playlist[i]['artist']) + ' - ' + str(playlist[i]['title']) + '\n' + 'Lien youtube : ' +str(playlist[i]['suggested_youtube_url'])).pack(padx=50, pady=50)
            tk.Label(frame, text=str(playlist[i]['lyrics'])).pack(padx=50, pady=20)
            can = Canvas(frame, width=300, height=10)
            can.create_line(0, 5, 300, 5)
            can.pack()
        # put the frame in the canvas
        canvas.create_window(0, 0, anchor='nw', window=frame)
        # make sure everything is displayed before configuring the scrollregion
        canvas.update_idletasks()
        canvas.configure(scrollregion=canvas.bbox('all'), yscrollcommand=scroll_y.set)    
        canvas.pack(fill='both', expand=True, side='left')
        scroll_y.pack(fill='y', side='right')

    def selec_fichier(self):
        filename = askopenfilename(title="Sélectionner un fichier .json",filetypes=[('all files','.*')])
        fichier = open(filename, "r")
        content = json.load(fichier)
        fichier.close()
        lire = Lecture_json(content, self.value.get())
        lire.nb_chansons_par_artiste()
        api = Requete_api(lire.liste)
        api.requeter_api()
        self.afficher_chansons(api.playlist)

    def lancement(self):
        # ouverture fenetre
        self.fenetre.title("Super créateur de playlist")
        #choix nombre chansons
        Label(self.fenetre, text="Choix du nombre de chansons :").pack()
        self.value.pack( padx = 5, pady=5)
        # Bouton import liste
        self.b.pack(padx = 30, pady=30)
        self.b.config(command = self.selec_fichier)
        # Playlist
        self.play.pack()

        self.fenetre.mainloop()