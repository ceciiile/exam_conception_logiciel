# Webservice / API

Cette API sert principalement à retourner une chanson aléatoire à partir d'un nom d'artiste. On obtient le nom de la chanson, les paroles (si trouvables) et le lien youtube recommandé (si disponible).

Lancer l'API :

```bash
uvicorn webservice.main:app
```

Vous pouvez à présent accéder à l'API depuis l'adresse : http://127.0.0.1:8000 .

Pour savoir si la connexion aux autres API se fait correctement : http://127.0.0.1:8000/ retourne false ou true.

Pour trouver une chanson aléatoire à partir d'un nom d'artiste : http://127.0.0.1:8000/random/{artiste}.


