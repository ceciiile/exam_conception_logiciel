# Client

L'objectif de cette application est de proposer à n'importe qui de créer une playlist aléatoire à partir d'une liste d'artistes. Dans cette liste, les artistes doivent être notés afin de favoriser ceux que l'utilisateur préfère.

## Interface

L'interface graphique se veut relativement simple d'utilisation, en particulier elle ne demande pas d'implémenter du code.

Quand l'interface s'ouvre, choisir le nombre de chansons dont on veut que la playlist soit constituée. Ensuite, cliquer sur "Choisir la liste des artistes" et sélectionner le document approprié dans ses fichiers. La liste doit être au format .json, contenir les noms d'artiste mais aussi leur note. Voir le document test.json pour un exemple.

Une fois le document choisi, la page peut mettre quelques secondes à afficher la playlist. Les musiques de la playlist sont les unes en-dessous des autres, utiliser la barre de défilement sur le côté droit pour toutes les voir.

## Pistes d'amélioration

Quand la playlist demandée est trop grande (plus d'une dizaine de chansons), l'application a tendance à planter. Un moyen de contourner le problème est de cliquer plusieurs fois sur "Choisir la liste des artistes" avec un nombre de musiques plus restreint.

La prise en compte de la notation des artistes n'est pas optimale. Il serait plus intéressant d'attribuer le nombre de chansons par artiste en pondérant par leur note.

