import pytest
from audiodb import Audiodb

def test_find_id():
    audio = Audiodb("oasis")
    audio.find_id()
    assert audio.artist_id == "111516"
