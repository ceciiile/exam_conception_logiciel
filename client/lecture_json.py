import json


class Lecture_json :

    def __init__(self, liste, nb_chansons):
        self.liste = liste
        self.nb_chansons = int(nb_chansons)
        
    def noter(self):
        dico = {}
        ordre = []
        for i in self.liste:
            dico[i["artiste"]] = i["note"]
        for k, v in sorted(dico.items(), key=lambda x: x[1] , reverse=True):
            ordre.append(k)
        self.liste = ordre
    
    def lister(self):
        nb_artists = len(self.liste)
        nouv_liste = []
        dico = {}
        i = 0
        for repet in range(0, nb_artists):
            nouv_liste.append(0)
        for repet in range(0, self.nb_chansons):
            nouv_liste[i] += 1
            if i == nb_artists-1 :
                i = 0
            else:
                i+=1
        for repet in range(0, nb_artists):
            dico[self.liste[repet]] = nouv_liste[repet]
        self.liste = dico

    def nb_chansons_par_artiste(self):
        self.noter()
        self.lister()