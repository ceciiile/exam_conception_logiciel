import requests 


class Requete_api :

    def __init__(self, liste_artists): 
        self.liste_artists = liste_artists
        self.playlist = []
    
    def requeter_api(self):
        for artiste in self.liste_artists.keys() :
            for repet in range(0, self.liste_artists[artiste]):
                research = "http://127.0.0.1:8000/random/" + str(artiste)
                song = requests.get(research)
                song = song.json()
                self.playlist.append(song)
