import requests 
import random

class Audiodb :
    """
    Cette classe permet de requêter l'API AudioDB .
    Nous nous en servons pour trouver un titre de chanson au hasard à partir d'un nom d'artiste.

    """

    def __init__(self, artist_name) :
        self.artist_name = artist_name
        self.artist_id = None
        self.album_id = None
        self.morceau_name = None
        self.yt_url = None


    def find_id(self):
        research = 'https://www.theaudiodb.com/api/v1/json/2/search.php?s=' + self.artist_name
        artist = requests.get(research)
        artist = artist.json()
        id = artist["artists"][0]["idArtist"]
        self.artist_id = id

    def find_random_album(self) :
        research = 'https://theaudiodb.com/api/v1/json/2/album.php?i=' + str(self.artist_id)
        albums = requests.get(research)
        albums = albums.json()
        nb_albums = len(albums["album"])
        nb_hasard = random.randint(0,nb_albums-1)
        album = albums["album"][nb_hasard]
        self.album_id = album["idAlbum"] 
    
    def find_random_song(self):
        research = 'https://theaudiodb.com/api/v1/json/2/track.php?m=' + str(self.album_id)
        morceaux = requests.get(research)
        morceaux = morceaux.json()
        nb_morceaux = len(morceaux['track'])
        nb_hasard = random.randint(0, nb_morceaux-1)
        morceau = morceaux["track"][nb_hasard]
        self.morceau_name = str(morceau["strTrack"])
        self.yt_url = str(morceau["strMusicVid"])


    def find_song_from_artist(self):
        self.find_id()
        self.find_random_album()
        self.find_random_song()