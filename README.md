# Super créateur de playlist

Cette application python est constituée de :
- un webservice/API qui propose aléatoirement une musique à partir d'un nom d'artiste.
- un client qui génère une playlist à partir d'une liste d'artiste.

## Quickstart

Installer les dépendances :

```bash
pip install -r requirements.txt
```

Ouvrir un terminal et lancer l'API : 

```bash
uvicorn webservice.main:app
```

Dans un autre terminal, lancer le script client:
```bash
python ./client/main.py
```

## Attention 

L'utilisation du client nécessite une liste d'artistes au fichier .json de la forme :
```bash
[{
    "artiste": "daft punk",
    "note": 18
},
{
    "artiste":"gloria gaynor",
    "note": 10
}]
```

## Tests

POur lancer les tests unitaires :

```bash
pytest
```

Pour l'instant il n'y a qu'un test.
