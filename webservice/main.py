import sys
import os
sys.path.append(os.getcwd())

import requests
from typing import Optional
from fastapi import FastAPI

from webservice.audiodb import Audiodb
from webservice.lyricsovh import Lyricsovh

app = FastAPI()


@app.get("/")
def read_root():
    res = requests.get('https://www.theaudiodb.com/api/v1/json/2/search.php?s=oasis')
    res2 = requests.get('https://api.lyrics.ovh/v1/oasis/wonderwall')
    if res :
        if res2 :
            return True
        else:
            return False
    else:
        return False

@app.get("/random/{artist_name}")
def find_song(artist_name: str):
    api = Audiodb(artist_name)
    api.find_song_from_artist()
    api2 = Lyricsovh(artist_name, api.morceau_name)
    api2.find_lyrics()
    return{"artist": artist_name, "title": api.morceau_name, "lyrics": api2.lyrics, "suggested_youtube_url": api.yt_url}