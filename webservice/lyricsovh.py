import requests 

class Lyricsovh :

    def __init__(self, artist_name, morceau_name):
        self.artist_name = artist_name
        self.morceau_name = morceau_name
        self.lyrics = None
    
    def find_lyrics(self):
        research = 'https://api.lyrics.ovh/v1/' + str(self.artist_name) + "/" + str(self.morceau_name)
        find = requests.get(research)
        find = find.json()
        if "lyrics" in find:    
            lyrics = find["lyrics"]
        else :
            lyrics = None
        self.lyrics = lyrics
    
